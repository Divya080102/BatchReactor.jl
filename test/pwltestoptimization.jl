using BatchReactor
using Fresa
# bounds
domain = Fresa.Domain(x -> BatchReactor.PiecewiseLinearProfile(zeros(length(x.ft)),-ones(length(x.fT)),BatchReactor.Tmin),
                      x -> BatchReactor.PiecewiseLinearProfile(ones(length(x.ft)),ones(length(x.fT)),BatchReactor.Tmax))
# create initial population consisting of just the initial profile
p0 = [Fresa.createpoint(BatchReactor.PiecewiseLinearProfile([0.5, 0.5, 0.5, 0.5], [0.0, 0.0, 0.0, 0.0], 323.0),
                        BatchReactor.objective, nothing, nothing)]
# invoke Fresa
nondominated, population = BatchReactor.solve(p0, domain)
println("Non-dominated solutions:")
println("$(population[nondominated])")
println("Full population:")
println("$population")
