using BatchReactor
using Fresa
# bounds
function lower(p :: BatchReactor.TemperatureProfile)
    if typeof(p) == BatchReactor.PiecewiseLinearProfile
        BatchReactor.PiecewiseLinearProfile(zeros(length(p.ft)),-ones(length(p.fT)),BatchReactor.Tmin)
    else
        BatchReactor.QuadraticSplineProfile(BatchReactor.Tmin, BatchReactor.Tmin, 0.25)
    end
end
function upper(p :: BatchReactor.TemperatureProfile)
    if typeof(p) == BatchReactor.PiecewiseLinearProfile
        BatchReactor.PiecewiseLinearProfile(ones(length(p.ft)),ones(length(p.fT)),BatchReactor.Tmax)
    else
        BatchReactor.QuadraticSplineProfile(BatchReactor.Tmax, BatchReactor.Tmax, 0.75)
    end
end
domain = Fresa.Domain(lower, upper)
# create initial population consisting of one initial profile for each type of representation
p0 = [Fresa.createpoint(BatchReactor.QuadraticSplineProfile(323.0, 323.0, 0.5), BatchReactor.objective, nothing, nothing)
      Fresa.createpoint(BatchReactor.PiecewiseLinearProfile([0.5, 0.5, 0.5, 0.5], [0.0, 0.0, 0.0, 0.0], 323.0),
                        BatchReactor.objective, nothing, nothing)]
# invoke Fresa
nondominated, population = BatchReactor.solve(p0, domain)
println("Non-dominated solutions:")
println("$(population[nondominated])")
println("Full population:")
println("$population")
