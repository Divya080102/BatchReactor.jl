using BatchReactor
using Fresa
# bounds
domain = Fresa.Domain(x -> BatchReactor.QuadraticSplineProfile(BatchReactor.Tmin, BatchReactor.Tmin, 0.25),
                      x -> BatchReactor.QuadraticSplineProfile(BatchReactor.Tmax, BatchReactor.Tmax, 0.75))
# create initial population consisting of just the initial profile
p0 = [Fresa.createpoint(BatchReactor.QuadraticSplineProfile(323.0, 323.0, 0.5),
                        BatchReactor.objective, nothing, nothing)]
# invoke Fresa
nondominated, population = BatchReactor.solve(p0, domain)
println("Non-dominated solutions:")
println("$(population[nondominated])")
println("Full population:")
println("$population")
