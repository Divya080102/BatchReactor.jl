using MyReactor
# define a temperature profile
profile = MyReactor.PiecewiseLinearProfile(
    [0.5 1.0])  # half the time in first interval
# simulate the process
results = MyReactor.simulation(profile)
println("Results from simulation: $results")
# # plot out the results, i.e. the concentrations of all species as a
# # function of time.
 using PyPlot
 PyPlot.plot(results.t, results.var[1])
 PyPlot.plot(results.t, results.var[2])
 PyPlot.plot(results.t, results.var[3])
 PyPlot.plot(results.t, results.var[4])
 PyPlot.xlabel("t")
 PyPlot.ylabel("Concentration")
 PyPlot.savefig("testsimulation.pdf")
